#!/usr/bin/env bash
dossier=`date +%Y%m%d%H%M%S`
echo "backup des anciennes versions compilées dans le dossier $dossier"
mkdir $dossier
if [ -d "./minetest" ];then
	echo "Déplacement de minetest"
	mv minetest $dossier/
fi

if [ -d "./subgames" ];then
	echo "Déplacement des subgames"
	mv subgames $dossier/
fi

if [ -d "./mods" ];then
	echo "Déplacement des mods"
	mv mods $dossier/
fi
mkdir subgames
mkdir mods
countfiles=$(ls -a $dossier | wc -w)
#echo $countfiles
if [ $countfiles -eq 2 ]
then
	echo "Pas de backup a faire."
	rm -r $dossier
fi
#echo "Déplacement de minetest_game"
#mv minetest_game $dossier/
#echo "Déplacement de MineClone2"
#mv MineClone2 $dossier/
#echo "Déplacement de aftermath"
#mv aftermath $dossier/
#echo "Déplacement de farlands"
#mv farlands $dossier/
#echo "Déplacement de Voxellar"
#mv Voxellar $dossier/
#echo "Déplacement de BFG"
#mv big_freaking_dig $dossier/
#echo "Déplacement de Minetest_TNG"
#mv Minetest_TNG $dossier/
#echo "Déplacement de minetest_defense"
#mv minetest_defense $dossier/
#echo "Déplacement de carbone-ng"
#mv carbone-ng $dossier
#echo "Déplacement de minetest-australopithecus"
#mv minetest-australopithecus $dossier
echo "Fin des backups"
echo "______________________________"
echo "Clonage du dépots git minetest"
echo "minetest..."
git clone --depth=1 https://github.com/minetest/minetest.git
echo "____________________"
echo "Clonage des subgames"
cd subgames
echo ""
echo "minetest_game..."
git clone --depth=1 https://github.com/minetest/minetest_game.git
echo ""
echo "MineClone2..."
git clone http://repo.or.cz/MineClone/MineClone2.git
echo ""
echo "AfterMath..."
git clone https://github.com/maikerumine/aftermath.git
echo ""
echo "Farlands..."
git clone https://github.com/D00Med/farlands.git
echo ""
echo "Voxellar..."
git clone https://github.com/azekillDIABLO/Voxellar.git
echo ""
echo "Big Freaking Dig..."
git clone https://github.com/Jordach/big_freaking_dig.git
echo ""
echo "Minetest TNG..."
git clone https://github.com/LNJ2/Minetest_TNG.git
echo ""
echo "minetest_defense"
git clone https://github.com/Kalabasa/minetest_defense.git
echo ""
echo "Carbone NG"
git clone https://github.com/Calinou/carbone-ng.git
echo ""
echo "australopithecus"
git clone https://github.com/minetest-australopithecus/minetest-australopithecus.git
echo ""
echo "trollblocks"
cp -r minetest_game trollblocks
cd trollblocks
git clone https://framagit.org/klm/minetest_lua_experiments.git
ln -s ../minetest_lua_experiments/trollblocks mods/trollblocks
rm game.conf
ln -s minetest_lua_experiments/game_trollblocks.conf game.conf
rm menu/icon.png
ln -s ../minetest_lua_experiments/icon.png menu/icon.png
rm menu/header.png
ln -s ../minetest_lua_experiments/header.png menu/header.png
echo ""
echo "_________________"
echo "Clonage des mods"
cd ../../mods
echo ""
echo "WorldEdit..."
git clone https://github.com/Uberi/Minetest-WorldEdit.git
echo ""
echo "_______________________"
echo "Compilation de minetest"
mkdir ../minetest/build/linux
cd ../minetest/build/linux
cmake ../../
make -j9
cd ../../
echo ""
echo "_______________________________"
echo "Linkage du dossier des subgames"
mv games games.old
ln -s ../subgames games
echo ""
echo "___________________________"
echo "Linkage du dossier des mods"
mv mods mods.old
ln -s ../mods mods
ln -s ~/.minetest/mods mods
echo ""
echo "___________________"
echo "Patching buggy mods"
cd ..
# génération des fichiers patchs selon ce modele: 
# dans le repertoire courant:
# diff -c subgames/MineClone2/mods/PLAYER/mcl_player/init.lua subgames/MineClone2/mods/PLAYER/mcl_player/init.lua.patched > MCL.mods.PLAYER.mcl_player.init.lua.patch
echo ""
echo "minetest_defense patch nodeupdate(p0) in mods/default/functions.lua"
patch -c -p0 -i defense.mods.default.functions.lua.patch
echo ""
echo ""
echo "minetest_TNG patch nodeupdate(p0) in mods/default/lua/environment/leafdecay.lua"
patch -c -p0 -i TNG.mods.default.lua.environment.leafdecay.lua.patch
echo ""
echo ""
echo "big_freaking_dig patch nodeupdate(p0) in mods/default/functions.lua"
patch -c -p0 -i bfd.mods.default.functions.lua.patch
echo ""
echo ""
echo "MineClone2 patch PLAYER in mods/PLAYER/mcl_player/init.lua"
patch -c -p0 -i MCL.mods.PLAYER.mcl_player.init.lua.patch
echo ""
echo ""
echo "minetest_defense removing buggy empty directory subgames/minetest_defense/mods/throwing"
rm -rf subgames/minetest_defense/mods/throwing
echo ""
echo "_______"
echo "FINI!!!"
